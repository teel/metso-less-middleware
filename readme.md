## Metso LESS Middleware

This is a custom version of less-middleware that is locked to LESS v1.3.0 for Metso Toolkit only. This is an ugly hack and should be fixed in the future.

For documentation see [less-middleware](https://npmjs.org/package/less-middleware).